import { strTransformedToDom } from "@kinda-ok/convenient/dist/strTransformedToDom.mjs";
import { fetchAsText } from '@kinda-ok/convenient/dist/fetchAsText.mjs'
import { micromark } from "micromark";
import { frontmatter, frontmatterHtml } from "micromark-extension-frontmatter";
import * as Yaml from "yaml";


/**
 * Transforms a markup string into a valid markup.
 * @returns
 */
export const markdownToMarkup = (markdownStr: string) => {
  const markup = micromark(markdownStr, {
    extensions: [frontmatter()],
    htmlExtensions: [frontmatterHtml()],
  });
  const header = Yaml.parseAllDocuments(markdownStr)[0];
  /** @type {Record<string, unknown>} */
  const data: Record<string, unknown> = header ? header.toJS() : {};
  return { markup, data };
};

/**
 * Loads and parses a markdown document. Makes use of micromark.
 * @param path the path to load
 */
export const fetchMarkdown = async (path: string) => {
  const raw = await fetchAsText(path)
  return markdownToDom(raw, path)
}


/**
 * Takes a an original string and transforms it into dom that can be slotted in a
 * document.
 * Additionally, it parses the frontmatter, and attempts to extract a title by finding either
 * the first title in the doc, or the filename (if provided).
 *
 * This is primarily there to allow to pass a markdown parser and get html, frontmatter, and
 * a title back.
 * 
 * @param rawStr
 * @param [path] the file path
 */
export const markdownToDom = strTransformedToDom(markdownToMarkup)