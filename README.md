# Convenient Markdown

A package to render markdown with frontmatter support

Uses [@kinda-ok/convenient](https://gitlab.com/kinda-ok/convenient)

It is stored individually because it 3 dependencies:

- [micromark](https://www.npmjs.com/package/micromark)
- [micromark-extension-frontmatter](https://www.npmjs.com/package/micromark-extension-frontmatter)
- [yaml](https://www.npmjs.com/package/yaml)